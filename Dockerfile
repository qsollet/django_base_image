FROM python:3.12-slim

LABEL maintainer=quentin@sollet.fr

COPY scripts/* /usr/local/bin/

RUN useradd --create-home --shell /bin/bash appuser
USER appuser
ENV PATH="/home/appuser/.local/bin:${PATH}"
RUN mkdir /home/appuser/src && mkdir /home/appuser/static && mkdir /home/appuser/media && mkdir /home/appuser/data

ENV HYPERCORN_BIND_IP 0.0.0.0
ENV HYPERCORN_BIND_PORT 8000
ENV HYPERCORN_WORKER 1

ENV STATIC_ROOT /home/appuser/static
ENV MEDIA_ROOT /home/appuser/media
ENV DATA_ROOT /home/appuser/data

ONBUILD USER appuser
ONBUILD COPY . /home/appuser/src
ONBUILD WORKDIR /home/appuser/src
ONBUILD RUN pre-build
ONBUILD RUN import-cron
ONBUILD RUN pip install --user hypercorn && pip install --user -r requirements.txt

EXPOSE 8080

CMD ["run-django"]
